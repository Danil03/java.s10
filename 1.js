let elem = document.getElementById('list').addEventListener('click', function(event) {

    if (event.target.tagName === 'LI') {
   
      const selectedId = event.target.id;
  
      const par = document.getElementById('par').children;
      for (let i = 0; i < par.length; i++) {
        par[i].style.display = 'none';
      }
  
      document.getElementById('tab' + selectedId.slice(-1)).style.display = 'block';
    }
  });